package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;

public class Drivetrain extends SubsystemBase
{
    private SpeedController[] talons = new SpeedController[4];
    private SpeedControllerGroup left;
    private SpeedControllerGroup right;
    private AHRS gyro;
    private XboxController controller;
    private double leftStick;
    private double rightStick;


    public Drivetrain()
    {
        talons[0] = new WPI_TalonSRX(0);
        talons[1] = new WPI_TalonSRX(1);
        talons[2] = new WPI_TalonSRX(2);
        talons[3] = new WPI_TalonSRX(3);
        left = new SpeedControllerGroup(talons[0], talons[1]);
        left.setInverted(true);
        right = new SpeedControllerGroup(talons[2], talons[3]);
        gyro = new AHRS();
        controller = new XboxController(0);
    }

    public void drive()
    {
        leftStick = controller.getY(GenericHID.Hand.kLeft);
        rightStick = controller.getY(GenericHID.Hand.kRight);

        left.set(leftStick);
        right.set(rightStick);
    }
}