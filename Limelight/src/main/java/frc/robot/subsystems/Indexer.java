package frc.robot.subsystems;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

public class Indexer
{
    private SpeedController indexer;
    private XboxController controller;

    public Indexer(int port, XboxController x)
    {
        indexer = new WPI_TalonSRX(port);
        controller = x;
    }
    
    public void toFlywheel() 
    {
        if(controller.getBumper(Hand.kRight)) 
        {
            indexer.set(1);
        } 
        else
        {
            indexer.stopMotor();
        }
        
    }
}