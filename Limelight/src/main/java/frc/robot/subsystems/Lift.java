package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

public class Lift extends SubsystemBase
{
    private WPI_TalonSRX lift;
    private XboxController controller;
    private double leftStick;

    public Lift(int port, XboxController x)
    {
        lift = new WPI_TalonSRX(port);
        controller = x;
    }

    public void spin()
    {

        if (controller.getYButton()){
            //set lift to certain height
        }
        else if (controller.getXButton()){
            //set lift to other height
        }
        else{
            leftStick = controller.getY(Hand.kLeft);
            lift.set(leftStick);
        }
    }

    
}