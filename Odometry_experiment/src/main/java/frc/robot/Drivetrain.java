package frc.robot;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj2.command.RamseteCommand;

/**
 * Represents a differential drive style drivetrain.
 */
public class Drivetrain {
  public static final double kMaxSpeed = 3.0; // meters per second
  public static final double kMaxAngularSpeed = 2 * Math.PI; // one rotation per second

  private static final double kTrackWidth = 0.381 * 2; // meters
  private static final double kWheelRadius = 0.1524; // meters
  private static final int kEncoderResolution = 4096;

  private final WPI_TalonSRX m_leftMaster = new WPI_TalonSRX(2);
  private final WPI_TalonSRX m_leftFollower = new WPI_TalonSRX(3);
  private final WPI_TalonSRX m_rightMaster = new WPI_TalonSRX(4);
  private final WPI_TalonSRX m_rightFollower = new WPI_TalonSRX(5);

  private final AHRS m_gyro = new AHRS();

  private final DifferentialDriveKinematics m_kinematics = new DifferentialDriveKinematics(kTrackWidth);

  private final DifferentialDriveOdometry m_odometry;
  private RamseteController someController;
  private RamseteCommand testCommand;
  /**
   * Constructs a differential drive object.
   * Sets the encoder distance per pulse and resets the gyro.
   */
  public Drivetrain() {
    m_leftFollower.follow(m_leftMaster);
    m_rightFollower.follow(m_rightMaster);
    m_gyro.reset();
    m_gyro.getRate();

    m_leftMaster.getSensorCollection().setQuadraturePosition(0, 100);
    m_rightMaster.getSensorCollection().setQuadraturePosition(0, 100);

    m_odometry = new DifferentialDriveOdometry(getAngle());
  }

  private Rotation2d getAngle() {
    double angleRadians = (m_gyro.getAngle()* Math.PI) / 180;
    return new Rotation2d(angleRadians);
  }

  /**
   * Sets the desired wheel speeds.
   *
   * @param speeds The desired wheel speeds.
   */
  public void setSpeeds(DifferentialDriveWheelSpeeds speeds) {
    m_leftMaster.set(ControlMode.Velocity, 
                      convertMetersPerSecToVelocityTics(speeds.leftMetersPerSecond));
    m_rightMaster.set(ControlMode.Velocity, 
                      convertMetersPerSecToVelocityTics(speeds.rightMetersPerSecond));
  }
  
  private double convertMetersPerSecToVelocityTics(double metersPerSecond) {
    // Convert from m/s to tics per 100 ms
    double convertedVal = (metersPerSecond * kEncoderResolution) / (10 * kWheelRadius);
    return convertedVal;
  }

  /**
   * Drives the robot with the given linear velocity and angular velocity.
   *
   * @param xSpeed Linear velocity in m/s.
   * @param rot    Angular velocity in rad/s.
   */
  @SuppressWarnings("ParameterName")
  public void drive(double xSpeed, double rot) {
    var wheelSpeeds = m_kinematics.toWheelSpeeds(new ChassisSpeeds(xSpeed, 0.0, rot));
    setSpeeds(wheelSpeeds);
  }

  private double convertDistanceTicsToMeters(double distanceTics) {
    double convertedVal = (distanceTics * kWheelRadius) / kEncoderResolution;
    return convertedVal;
  }

  /**
   * Updates the field-relative position.
   */
  public void updateOdometry() {
    double leftTics = m_leftMaster.getSensorCollection().getQuadraturePosition();
    double rightTics = m_rightMaster.getSensorCollection().getQuadraturePosition();
    double leftDistance = convertDistanceTicsToMeters(leftTics);
    double rightDistance = convertDistanceTicsToMeters(rightTics);
    Pose2d pose = m_odometry.update(getAngle(), leftDistance, rightDistance);
    pose.getTranslation();
  }
}