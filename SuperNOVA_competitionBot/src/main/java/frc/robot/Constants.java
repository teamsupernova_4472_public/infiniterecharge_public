/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    /**
     * Gyro PID constants.
     */
    public static final double GYRO_P = 0.019;
    public static final double GYRO_I = 0.0;
    public static final double GYRO_D = 0.002;
    public static final double LIMELIGHT_P = 0.06;
    public static final double LIMELIGHT_I = 0.01;
    public static final double LIMELIGHT_D = 0.008;
    public static final double GYRO_SPEED_P = 0; //0.005;
    public static final double GYRO_SPEED_I = 0; //0.02;
    public static final double GYRO_SPEED_D = 0; //0.0001;
    public static final double MAX_DRIVE_SPEED = 3800;
    public static final double MAX_FLY_WHEEL_SPEED = 131000;
    public static final double DISTANCE_K = 15.16575;
    public static final double HEIGHT_OF_GOAL = 8.1875;
    public static final double LIMELIGHT_MOUNTING_HEIGHT = 1.5;
    public static final double ENCODER_TICS_PER_REVOLUTION = 4096;
    public static final double WHEEL_CIRCUMFERENCE = 6.0 * Math.PI;
    public static final double ENCODER_TICS_PER_INCH = ENCODER_TICS_PER_REVOLUTION / WHEEL_CIRCUMFERENCE;
    public static final double IN_SEC_TO_TICS_TENTH = ENCODER_TICS_PER_INCH / 10;
    public static final double ENCODER_TICS_PER_FLYWHEEL_REVOLUTION = 12272;
    public static final double GRAVITY = 32;
}
