/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import java.util.HashMap;
import java.util.Map;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.kauailabs.navx.frc.AHRS;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.DriveForwardSpeed;
import frc.robot.commands.DriveForwardSpeedPosGyro;
import frc.robot.commands.LimeLightTurn;
import frc.robot.commands.autopaths.EnemyTrenchAuton;
import frc.robot.commands.autopaths.ShootDriveForwardAuton;
import frc.robot.commands.autopaths.TargetTrenchAutoPath;
import frc.robot.commands.controllers.ColorWheelController;
import frc.robot.commands.controllers.DriveTrainController;
import frc.robot.commands.controllers.FlyWheelController;
import frc.robot.commands.controllers.IndexerController;
import frc.robot.commands.controllers.IntakeController;
import frc.robot.commands.controllers.LiftController;
import frc.robot.commands.controllers.PneumaticHoodController;
import frc.robot.subsystems.ColorWheel;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Lift;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.PneumaticHood;
import frc.robot.subsystems.Sensors;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  // CAN bus declaration
  private final Compressor compressor = new Compressor(0);
  private final PowerDistributionPanel pdp = new PowerDistributionPanel(1);
  private final WPI_TalonSRX leftEncoder = new WPI_TalonSRX(2);
  private final WPI_TalonSRX left = new WPI_TalonSRX(3);
  private final WPI_TalonSRX rightEncoder = new WPI_TalonSRX(4);
  private final WPI_VictorSPX right = new WPI_VictorSPX(5);
  private final WPI_TalonSRX flyWheelEncoder = new WPI_TalonSRX(6);
  private final WPI_TalonSRX flyWheelFollow = new WPI_TalonSRX(7);
  private final WPI_TalonSRX indexerMotor = new WPI_TalonSRX(8);
  private final WPI_VictorSPX indexerFollower = new WPI_VictorSPX(10);
  private final WPI_TalonSRX intakeMain = new WPI_TalonSRX(9);
  private final DoubleSolenoid intakeActuator = new DoubleSolenoid(0, 4, 5);
  private final CANSparkMax elevatorClimbMotor = new CANSparkMax(11, MotorType.kBrushless);
  private final WPI_TalonSRX elevatorWheel = new WPI_TalonSRX(12);
  private final DoubleSolenoid pneumaticLatch = new DoubleSolenoid(0, 2, 3);
  private final WPI_TalonSRX colorWheelMotor = new WPI_TalonSRX(13);
  private final ColorWheel colorWheel = new ColorWheel(colorWheelMotor);

  // Other
  private final AHRS gyro = new AHRS();

  // Controllers
  private final XboxController driver = new XboxController(0);
  private final XboxController partner = new XboxController(1);

  // Subsystems
  private final Sensors sensors = new Sensors(leftEncoder, rightEncoder, flyWheelEncoder, elevatorClimbMotor, gyro, pdp, colorWheel);
  private final LimeLight limeLight = new LimeLight();
  private final DriveTrain driveTrain = new DriveTrain(leftEncoder, rightEncoder);
  private final FlyWheel flyWheel = new FlyWheel(flyWheelEncoder);
  private final Indexer indexer = new Indexer(indexerMotor);
  private final Intake intake = new Intake(intakeMain, intakeActuator);
  private final Lift lift = new Lift(elevatorClimbMotor, elevatorWheel, pneumaticLatch);
  private final PneumaticHood ph = new PneumaticHood(new DoubleSolenoid(0, 6, 7));


  // Commands
  private final DriveForwardSpeed driveForwardSpeed = new DriveForwardSpeed(driveTrain, 1000, 10000);
  private final LimeLightTurn limeLightTurn;
  private final DriveTrainController drive = new DriveTrainController(driveTrain, driver);
  private final FlyWheelController flyWheelController = new FlyWheelController(flyWheel, 0.85 * Constants.MAX_FLY_WHEEL_SPEED, driver, partner, limeLight);
  private final IndexerController indexerController = new IndexerController(indexer, driver, partner);
  private final IntakeController intakeController = new IntakeController(driver, partner, intake, 0.6);
  private final LiftController liftController = new LiftController(lift, partner);
  private final PneumaticHoodController hoodController = new PneumaticHoodController(ph, partner);
  private final SendableChooser<Command> chooser;
  private final TargetTrenchAutoPath trenchAuto = new TargetTrenchAutoPath(driveTrain, sensors, limeLight, flyWheel, indexer, intake, ph);
  private final ShootDriveForwardAuton shootDriveForwardAuto = new ShootDriveForwardAuton(driveTrain, limeLight, sensors, flyWheel, indexer, intake, ph);
  private final ColorWheelController colorWheelController = new ColorWheelController(colorWheel, 0.5, partner, ColorWheel.getColor());
  private final EnemyTrenchAuton enemyTrenchAuton = new EnemyTrenchAuton(driveTrain, sensors, limeLight, flyWheel, indexer, intake, ph);
  private final DriveForwardSpeedPosGyro driveForwardSpeedPosGyro = new DriveForwardSpeedPosGyro(driveTrain, sensors, 
                                                                        50 * Constants.IN_SEC_TO_TICS_TENTH, -Double.MAX_VALUE, Double.MAX_VALUE);

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    Map<String, NetworkTableEntry> gyroTuningTab = createGyroTuningTab();
    NetworkTableEntry pEntry = gyroTuningTab.get("P");
    NetworkTableEntry iEntry = gyroTuningTab.get("I");
    NetworkTableEntry dEntry = gyroTuningTab.get("D");
    limeLightTurn = new LimeLightTurn(driveTrain, limeLight, sensors, Integer.MAX_VALUE);
    chooser = new SendableChooser<Command>();
    left.setInverted(false);
    leftEncoder.setInverted(false);

    right.setInverted(false);
    rightEncoder.setInverted(false);
    leftEncoder.setSensorPhase(false);
    rightEncoder.setSensorPhase(true);
    leftEncoder.getSensorCollection().setQuadraturePosition(0, 100);
    rightEncoder.getSensorCollection().setQuadraturePosition(0, 100);

    flyWheelFollow.setInverted(true);
    left.follow(leftEncoder);
    right.follow(rightEncoder);
    flyWheelFollow.follow(flyWheelEncoder);
    intakeMain.setInverted(true);
    indexerFollower.follow(indexerMotor);

    setCurrentLimits(leftEncoder, 30);
    setCurrentLimits(left, 30);
    setCurrentLimits(rightEncoder, 30);
    setCurrentLimits(flyWheelEncoder, 60);
    setCurrentLimits(flyWheelFollow, 60);
    setCurrentLimits(indexerMotor, 40);
    setCurrentLimits(intakeMain, 40);
    setCurrentLimits(elevatorWheel, 40);
    setCurrentLimits(colorWheelMotor, 15);

    CommandScheduler.getInstance().setDefaultCommand(driveTrain, drive);
    CommandScheduler.getInstance().setDefaultCommand(flyWheel, flyWheelController);
    CommandScheduler.getInstance().setDefaultCommand(indexer, indexerController);
    CommandScheduler.getInstance().setDefaultCommand(intake, intakeController);
    CommandScheduler.getInstance().setDefaultCommand(lift, liftController);
    CommandScheduler.getInstance().setDefaultCommand(ph, hoodController);
    CommandScheduler.getInstance().setDefaultCommand(colorWheel, colorWheelController);
    //TODO: Color Picker (EVERYTHING)
    ShuffleboardTab drivingTab = Shuffleboard.getTab("Driving");
    chooser.setDefaultOption("Shoot and drive forward auto", shootDriveForwardAuto);
    chooser.addOption("Trench auto", trenchAuto);
    chooser.addOption("Enemy Trench auto", enemyTrenchAuton);
    drivingTab.add("Auton Selection", chooser).withSize(4, 2).withPosition(0, 0)
        .withWidget(BuiltInWidgets.kSplitButtonChooser);
    drivingTab.add("Power Distribution Panel", pdp).withSize(4, 2).withPosition(0, 2);
    // Configure the button bindings
    configureButtonBindings();
  }

  private void setCurrentLimits(WPI_TalonSRX talon, int amps) {
    talon.configContinuousCurrentLimit(amps, 100);
    talon.configPeakCurrentDuration(0,100);
    talon.configPeakCurrentLimit(0, 100);
    talon.enableCurrentLimit(true);
  }

  private static Map<String, NetworkTableEntry> createGyroTuningTab() {
    Map<String, NetworkTableEntry> tuningEntries = new HashMap<>();
    ShuffleboardTab gyroTuningTab = Shuffleboard.getTab("Gyro Tuning");
    NetworkTableEntry pEntry = gyroTuningTab.add("P", Constants.GYRO_P).getEntry();
    NetworkTableEntry iEntry = gyroTuningTab.add("I", Constants.GYRO_I).getEntry();
    NetworkTableEntry dEntry = gyroTuningTab.add("D", Constants.GYRO_D).getEntry();
    tuningEntries.put("P", pEntry);
    tuningEntries.put("I", iEntry);
    tuningEntries.put("D", dEntry);
    return tuningEntries;
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    JoystickButton driverA = new JoystickButton(driver, Button.kA.value);
    driverA.whenHeld(limeLightTurn);
    JoystickButton driverB = new JoystickButton(driver, Button.kB.value);
    driverB.whenHeld(driveForwardSpeedPosGyro);
  }


  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return chooser.getSelected();
  }

  public Compressor getCompressor() {
    return compressor;
  }

  public void outputSensors() {
    sensors.outputSensors();
  }

  public void resetSensors() {
    leftEncoder.getSensorCollection().setQuadraturePosition(0, 100);
    rightEncoder.getSensorCollection().setQuadraturePosition(0, 100);
    flyWheelEncoder.getSensorCollection().setQuadraturePosition(0, 100);
    gyro.reset();
  }

  public void disableCameraLed() {
    limeLight.setLedMode(1);
  }

  public void enableCameraLed() {
    limeLight.setLedMode(0);
  }
}
