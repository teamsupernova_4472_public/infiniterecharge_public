package frc.robot;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class RobotUtils {
    public static double deadband(double deadband, double input) {
        double output = input;
        if(Math.abs(input) < deadband) {
            output = 0;
        }
        SmartDashboard.putNumber("Dead band", output);
        return output;
    }
}