package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.FlyWheel;

public class AutonFlyWheel extends CommandBase{

    private final FlyWheel flyWheel;
    private double speed;

    public AutonFlyWheel(FlyWheel flyWheel, double speed){
        this.flyWheel = flyWheel;
        this.speed = speed;
        addRequirements(flyWheel);
    }

    @Override
    public void initialize() {
        flyWheel.flyWheelMotor.selectProfileSlot(1, 0);
        flyWheel.flyWheelMotor.set(ControlMode.Velocity, speed*Constants.MAX_FLY_WHEEL_SPEED);
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
