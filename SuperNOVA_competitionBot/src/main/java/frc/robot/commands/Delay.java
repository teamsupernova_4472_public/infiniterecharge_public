/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;


import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * A delay command to do nothing for a set time period.
 */
public class Delay extends CommandBase {
  
  private final int delayMs;
  private final Timer timer;

  public Delay(int delayMs) {
    this.delayMs = delayMs;
    this.timer = new Timer();
  }

  // Called just before this Command runs the first time
  @Override
  public void initialize() {
    timer.reset();
    timer.start();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
      timer.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    double currentTimeMS = timer.get()*1000;
    boolean isFinished = currentTimeMS > delayMs;
    return isFinished;
  }
}