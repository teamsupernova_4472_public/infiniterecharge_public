
package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class DriveForwardSpeed extends CommandBase {
    private final DriveTrain drive;
    private final int speed;
    private final int timeOut;
    private final Timer timer =  new Timer();

    /**
     * Contructor
     * 
     * @param drive The drive train sub system
     * @param speed The speed to drive the robot.
     * @param timeOut the amount of time to drive at the given speed.
     */
    public DriveForwardSpeed(DriveTrain drive, int speed, int timeOut) {
        this.drive = drive;
        this.speed = speed;
        this.timeOut = timeOut;
        addRequirements(drive);        
    }

    @Override
    public void initialize(){
       timer.reset();
       timer.start();
       drive.left.selectProfileSlot(1, 0);
       drive.right.selectProfileSlot(1, 0);
       drive.left.getSensorCollection().setQuadraturePosition(0, 100);
       drive.right.getSensorCollection().setQuadraturePosition(0, 100);
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        drive.drive(0, 0);
    }

    @Override
    public void execute() {
        drive.autonDriveForwardSpeed(speed);
    }

    @Override
    public boolean isFinished() {
      double currentTimeMS = timer.get()*1000;
      boolean isFinished = currentTimeMS > timeOut;
      return isFinished;
    }
}