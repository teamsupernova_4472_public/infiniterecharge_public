
package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpiutil.math.MathUtil;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Sensors;

public class DriveForwardSpeedPosGyro extends CommandBase {
    private static final double STOP_PERCENT = 0.9;
    private final DriveTrain drive;
    private final PIDController gyroPid = new PIDController(16.0, 0.0, 16);
    private final double speed;
    private final double position;
    private final double timeOut;
    private final Sensors sensor;
    private final Timer timer =  new Timer();

    /**
     * Contructor
     * 
     * @param drive The drive train sub system
     * @param speed The speed to drive the robot.
     * @param position The position for the robot to reach.
     * @param timeOut the amount of time give to drive to target.
     */
    public DriveForwardSpeedPosGyro(DriveTrain drive, Sensors sensor, double speed, double position, double timeOut) {
        this.drive = drive;
        this.position = position;
        this.speed = position > 0 ? Math.abs(speed) : -Math.abs(speed);
        this.timeOut = timeOut;
        this.sensor = sensor;
        addRequirements(drive);        
    }

    @Override
    public void initialize(){
       timer.reset();
       timer.start();
       drive.left.getSensorCollection().setQuadraturePosition(0, 100);
       drive.right.getSensorCollection().setQuadraturePosition(0, 100);
       sensor.gyro.reset();
       gyroPid.setSetpoint(0.0);
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        drive.drive(0, 0);
    }

    @Override
    public void execute() {
        double currentPosition = drive.getPosition();
        double heading = sensor.gyro.getAngle();
        if((position > 0 && currentPosition < STOP_PERCENT * position) ||
            (position <= 0 && currentPosition > STOP_PERCENT * position)) {
            double gyroError = gyroPid.calculate(heading);
            double headingCorrection = -gyroError;
            double leftSpeed = MathUtil.clamp(speed - headingCorrection, -Constants.MAX_DRIVE_SPEED, Constants.MAX_DRIVE_SPEED);
            double rightSpeed = MathUtil.clamp(speed + headingCorrection, -Constants.MAX_DRIVE_SPEED, Constants.MAX_DRIVE_SPEED);
            drive.autonDriveForwardSpeed(leftSpeed , rightSpeed);
        } else {
            drive.autonDriveForward(position);
        }
    }

    @Override
    public boolean isFinished() {
      double currentTimeMS = timer.get()*1000;
      boolean isFinished = currentTimeMS > timeOut;
      return isFinished;
    }
}