
package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import edu.wpi.first.wpiutil.math.MathUtil;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.Sensors;

import com.kauailabs.navx.frc.AHRS;


public class LimeLightTurn extends PIDCommand{
    private final Timer timer = new Timer();
    private final int timeout;
    private final PIDController controller;
    private final NetworkTableEntry pEntry;
    private final NetworkTableEntry iEntry;
    private final NetworkTableEntry dEntry;
    private final DriveTrain driveTrain;
    public final AHRS gyro;
    public double limeTurnValue;
    private final LimeLight limeLight;

    public LimeLightTurn(DriveTrain driveTrain, LimeLight limeLight, Sensors sensor, int timeout) {
        this(driveTrain, limeLight, sensor, timeout, null, null, null);
    }

    
    public LimeLightTurn(DriveTrain driveTrain, LimeLight limeLight, Sensors sensor, int timeout,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        this(driveTrain, limeLight, sensor, timeout, new PIDController(Constants.LIMELIGHT_P, Constants.LIMELIGHT_I, Constants.LIMELIGHT_D), 
                pEntry, iEntry, dEntry);
    }

    private LimeLightTurn(DriveTrain driveTrain, LimeLight limeLight, Sensors sensor, int timeout, PIDController controller,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        super(controller, //<-- PID Controller
            limeLight::getXDegreesAwayFromTarget, // <-- access to gyro angle method
            0, //angle you want to go to
            output -> {
                double clamped = MathUtil.clamp(output, -1, 1);
                driveTrain.left.set(ControlMode.PercentOutput,  clamped);
                driveTrain.right.set(ControlMode.PercentOutput, -clamped);
            }, //<--output command on robot
            driveTrain, //drive train requirement
            limeLight//sensor requirement
            );
        this.gyro = sensor.gyro;
        this.timeout = timeout;
        this.controller = controller;
        this.pEntry = pEntry;
        this.iEntry = iEntry;
        this.dEntry = dEntry;
        this.driveTrain = driveTrain;
        limeTurnValue = 0;
        this.limeLight = limeLight;
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
        controller.reset();
        driveTrain.left.selectProfileSlot(1, 0);
        driveTrain.right.selectProfileSlot(1, 0);
        if(pEntry != null && iEntry != null && dEntry != null) {
            controller.setP(pEntry.getDouble(0));
            controller.setI(iEntry.getDouble(0));
            controller.setD(dEntry.getDouble(0));
        }
        limeLight.setLedMode(0);
        gyro.reset();
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        driveTrain.drive(0, 0);
        limeTurnValue = gyro.getAngle();
        if(interrupted) {
            limeLight.setLedMode(1);
        }
    }

    @Override
    public boolean isFinished() {
        boolean isFinished = timeout < timer.get() * 1000;
        return isFinished;
    }
     
}