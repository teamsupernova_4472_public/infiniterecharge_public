package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.PneumaticHood;

public class SetHood extends CommandBase {
    private final PneumaticHood hood;
    private final Value hoodValue;
    private final Timer timer;
    private final double delayMs;

    public SetHood(PneumaticHood hood, Value hoodValue, double timeout) {
        this.hood = hood;
        this.hoodValue = hoodValue;
        this.delayMs = timeout;
        timer = new Timer();
        addRequirements(hood);
    }

    @Override
    public void initialize() {
        timer.reset();
        timer.start();
        hood.hoodPiston.set(Value.kOff);
    }

      // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        SmartDashboard.putString("Auto Intake Value", hoodValue.name());
        hood.hoodPiston.set(hoodValue);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        timer.stop();
    }


      // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        double currentTimeMS = timer.get()*1000;
        boolean isFinished = currentTimeMS > delayMs;
        return isFinished;
    }
}