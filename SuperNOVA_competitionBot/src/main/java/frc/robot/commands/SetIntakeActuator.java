package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Intake;

public class SetIntakeActuator extends CommandBase {
    private final Intake intake;
    private final Value intakeValue;
    private final Timer timer;
    private final double delayMs;

    public SetIntakeActuator(Intake intake, Value intakeValue, double timeout) {
        this.intake = intake;
        this.intakeValue = intakeValue;
        this.delayMs = timeout;
        timer = new Timer();
        addRequirements(intake);
    }

    @Override
    public void initialize() {
        timer.reset();
        timer.start();
        intake.intakeActuator.set(Value.kOff);
    }

      // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        SmartDashboard.putString("Auto Intake Value", intakeValue.name());
        intake.intakeActuator.set(intakeValue);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        timer.stop();
    }


      // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        double currentTimeMS = timer.get()*1000;
        boolean isFinished = currentTimeMS > delayMs;
        return isFinished;
    }
}