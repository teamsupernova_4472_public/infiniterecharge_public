
package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import edu.wpi.first.wpiutil.math.MathUtil;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Sensors;


public class Turn extends PIDCommand{
    private final Sensors sensor;
    private final Timer timer = new Timer();
    private final int timeout;
    private final PIDController controller;
    private final NetworkTableEntry pEntry;
    private final NetworkTableEntry iEntry;
    private final NetworkTableEntry dEntry;
    private final DriveTrain driveTrain;

    public Turn(DriveTrain driveTrain, Sensors sensor, double angle, int timeout) {
        this(driveTrain, sensor, angle, timeout, null, null, null);
    }

    
    public Turn(DriveTrain driveTrain, Sensors sensor, double angle, int timeout,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        this(driveTrain, sensor, angle, timeout, new PIDController(Constants.GYRO_P, Constants.GYRO_I, Constants.GYRO_D), 
                pEntry, iEntry, dEntry);
    }

    private Turn(DriveTrain driveTrain, Sensors sensor, double angle, int timeout, PIDController controller,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        super(controller, //<-- PID Controller
            sensor.gyro::getAngle, // <-- access to gyro angle method
            angle, //angle you want to go to
            output -> {
                double clamped = MathUtil.clamp(output, -1, 1);
                driveTrain.left.set(ControlMode.PercentOutput, -clamped);
                driveTrain.right.set(ControlMode.PercentOutput, clamped);
            }, //<--output command on robot
            driveTrain, //drive train requirement
            sensor//sensor requirement
            );
        this.sensor = sensor;
        this.timeout = timeout;
        this.controller = controller;
        this.pEntry = pEntry;
        this.iEntry = iEntry;
        this.dEntry = dEntry;
        this.driveTrain = driveTrain;
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
        sensor.gyro.reset();
        controller.reset();
        driveTrain.left.selectProfileSlot(1, 0);
        driveTrain.right.selectProfileSlot(1, 0);
        if(pEntry != null && iEntry != null && dEntry != null) {
            controller.setP(pEntry.getDouble(0));
            controller.setI(iEntry.getDouble(0));
            controller.setD(dEntry.getDouble(0));
        }
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        driveTrain.drive(0, 0);
    }

    @Override
    public boolean isFinished() {
        boolean isFinished = timeout < timer.get() * 1000;
        return isFinished;
    }
     
}