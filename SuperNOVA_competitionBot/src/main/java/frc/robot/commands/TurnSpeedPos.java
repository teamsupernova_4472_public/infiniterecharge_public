
package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Sensors;

public class TurnSpeedPos extends CommandBase {
    private static final double STOP_VELOCITY_ANGLE = 27;
    private static final double FEEDFORWARD = 5.0;
    private static final double LIME_ANGLE_OFFSET = -5.0;
    private final LimeLightTurn limeLightTurn;
    private final DriveTrain drive;
    private final Sensors sensor;
    private final double speed;
    private double angle;
    private final int timeOut;
    private final Timer timer =  new Timer();
    private final PIDController positionPidController;

    /**
     * Contructor
     * 
     * @param drive The drive train sub system
     * @param sensor The sensor subsystem.
     * @param angularSpeed The speed to turn the robot in degrees per sec.
     * @param angle The position to turn to in degrees.
     * @param timeOut the amount of time to drive at the given speed.
     */
    public TurnSpeedPos(DriveTrain drive, Sensors sensor, int angularSpeed, double angle, int timeOut) {
        this(drive, sensor, null, angularSpeed, angle, timeOut);
    }

    /**
     * Contructor
     * 
     * @param drive The drive train sub system
     * @param sensor The sensor subsystem.
     * @param limeLightTurn The prior limelight turn command to determine the additional turn angle.
     * @param angularSpeed The speed to turn the robot in degrees per sec.
     * @param angle The position to turn to in degrees.
     * @param timeOut the amount of time to drive at the given speed.
     */
    public TurnSpeedPos(DriveTrain drive, Sensors sensor, LimeLightTurn limeLightTurn,
                         int angularSpeed, double angle, int timeOut) {
        this.drive = drive;
        this.sensor = sensor;
        this.speed = Math.abs(angularSpeed);
        this.limeLightTurn = limeLightTurn;
        this.angle = angle;
        this.timeOut = timeOut;
        positionPidController = new PIDController(Constants.GYRO_P, Constants.GYRO_I, Constants.GYRO_D);
        addRequirements(drive);        
    }

    @Override
    public void initialize(){
       timer.reset();
       timer.start();
       sensor.gyro.reset();
       drive.left.getSensorCollection().setQuadraturePosition(0, 100);
       drive.right.getSensorCollection().setQuadraturePosition(0, 100);
       if(limeLightTurn != null) {
            double limeOffset;
            if(limeLightTurn.limeTurnValue > 0) {
                limeOffset = limeLightTurn.limeTurnValue - LIME_ANGLE_OFFSET;
            } else {
                limeOffset = limeLightTurn.limeTurnValue + LIME_ANGLE_OFFSET;
            }
            this.angle = angle - limeOffset;
       } 
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        drive.drive(0, 0);
    }

    @Override
    public void execute() {
        double currentAngle = sensor.gyro.getAngle();
        double angleDiff = angle - currentAngle;
        SmartDashboard.putNumber("Angle diff", angleDiff);
        if((angle > 0 &&  (angleDiff) > STOP_VELOCITY_ANGLE) || 
            (angle < 0 && (angleDiff) < -STOP_VELOCITY_ANGLE)) {
            double turnSpeed = speed *FEEDFORWARD;
            if(angle < 0) {
                turnSpeed *= -1;
            }
            drive.turnSpeed(turnSpeed);
        } else {
            double pidOutput = positionPidController.calculate(currentAngle, angle);
            drive.turn(pidOutput);
        }
    }

    @Override
    public boolean isFinished() {
      double currentTimeMS = timer.get()*1000;
      boolean isFinished = currentTimeMS > timeOut;
      return isFinished;
    }
}