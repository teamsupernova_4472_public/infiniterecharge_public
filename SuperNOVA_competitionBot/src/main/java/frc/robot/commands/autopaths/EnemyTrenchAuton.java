package frc.robot.commands.autopaths;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.AutonFlyWheel;
import frc.robot.commands.AutonIndexer;
import frc.robot.commands.AutonIntake;
import frc.robot.commands.Delay;
import frc.robot.commands.DriveForwardSpeedPos;
import frc.robot.commands.DriveForwardSpeedPosGyro;
import frc.robot.commands.LimeLightTurn;
import frc.robot.commands.SetHood;
import frc.robot.commands.SetIntakeActuator;
import frc.robot.commands.Turn;
import frc.robot.commands.TurnSpeedPos;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.PneumaticHood;
import frc.robot.subsystems.Sensors;

/**
 * A safe auto that has the robot shoot 3 balls and drive across the initiation line
 */
public class EnemyTrenchAuton extends SequentialCommandGroup {


    public EnemyTrenchAuton(DriveTrain driveTrain, Sensors sensors, LimeLight limeLight, FlyWheel flyWheel, 
                            Indexer indexer, Intake intakeSystem, PneumaticHood hood) {
        addCommands(
            new AutonFlyWheel(flyWheel, 0.7), // Set starting flywheel distance
            new ParallelCommandGroup(
                new SetHood(hood, Value.kForward, 1500), // Actuate the Hood and intake pistons
                new SetIntakeActuator(intakeSystem, Value.kForward, 1500)
             ),
            new ParallelCommandGroup(
                new AutonIntake(intakeSystem, 3700, 0.5),
                new SequentialCommandGroup(
                    new DriveForwardSpeedPosGyro(driveTrain, sensors, 50 * Constants.IN_SEC_TO_TICS_TENTH, 85 * Constants.ENCODER_TICS_PER_INCH, 2500),
                    new DriveForwardSpeedPosGyro(driveTrain, sensors, 20 * Constants.IN_SEC_TO_TICS_TENTH, 3 * Constants.ENCODER_TICS_PER_INCH, 200),
                    new Turn(driveTrain, sensors, 40, 1000)
                )   
            ),
            new ParallelCommandGroup(
                new AutonIntake(intakeSystem, 11300, 0.4),
                new SequentialCommandGroup(
                    new DriveForwardSpeedPosGyro(driveTrain, sensors, 50 * Constants.IN_SEC_TO_TICS_TENTH, -100 * Constants.ENCODER_TICS_PER_INCH, 3000),
                    new TurnSpeedPos(driveTrain, sensors, 120, 190, 1700),
                    new LimeLightTurn(driveTrain, limeLight, sensors, 1500),
                    new AutonIndexer(indexer, 5000) //shoot balls from trench
                )
            )
        );
    }

    

}
