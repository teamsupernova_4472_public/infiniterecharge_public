package frc.robot.commands.autopaths;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.AutonFlyWheel;
import frc.robot.commands.AutonIndexer;
import frc.robot.commands.AutonIntake;
import frc.robot.commands.DriveForwardSpeedPos;
import frc.robot.commands.LimeLightTurn;
import frc.robot.commands.SetHood;
import frc.robot.commands.SetIntakeActuator;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.PneumaticHood;
import frc.robot.subsystems.Sensors;

/**
 * A safe auto that has the robot shoot 3 balls and drive across the initiation line
 */
public class ShootDriveForwardAuton extends SequentialCommandGroup {


    public ShootDriveForwardAuton(DriveTrain driveTrain, LimeLight limeLight, Sensors sensor, FlyWheel flyWheel, 
                                Indexer indexer, Intake intakeSystem, PneumaticHood hood) {
        addCommands(
            new AutonFlyWheel(flyWheel, 0.7), // Set starting flywheel distance
            new ParallelCommandGroup(
                new SetHood(hood, Value.kForward, 500), // Actuate the Hood and intake pistons
                new SetIntakeActuator(intakeSystem, Value.kForward, 500)
             ),
            new LimeLightTurn(driveTrain, limeLight, sensor, 1500), //turns to face hole
            new ParallelCommandGroup(
                new AutonIndexer(indexer, 1500), //lets preloads into flywheel to shoot them
                new AutonIntake(intakeSystem, 1500, 0.5)
            ),
            new DriveForwardSpeedPos(driveTrain, 50 * Constants.IN_SEC_TO_TICS_TENTH, 30 * Constants.ENCODER_TICS_PER_INCH, 4000)
        );
    }

    

}
