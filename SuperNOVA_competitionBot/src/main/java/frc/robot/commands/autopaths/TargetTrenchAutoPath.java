package frc.robot.commands.autopaths;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.AutonFlyWheel;
import frc.robot.commands.AutonIndexer;
import frc.robot.commands.AutonIntake;
import frc.robot.commands.Delay;
import frc.robot.commands.DriveForwardSpeedPosGyro;
import frc.robot.commands.LimeLightTurn;
import frc.robot.commands.SetHood;
import frc.robot.commands.SetIntakeActuator;
import frc.robot.commands.Turn;
import frc.robot.commands.TurnSpeedPos;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.PneumaticHood;
import frc.robot.subsystems.Sensors;

public class TargetTrenchAutoPath extends SequentialCommandGroup {


    public TargetTrenchAutoPath(DriveTrain driveTrain, Sensors sensors, LimeLight limeLight, FlyWheel flyWheel, 
                                Indexer indexer, Intake intakeSystem, PneumaticHood hood) {
        // Declare our limelight turn and turn to face trench early in order to pass in the limelight turn into the trench turn command.
        // This will allow the trench turn to read in the prior heading achieved from the limelight turn
        //LimeLightTurn initialLimeLightAim = new LimeLightTurn(driveTrain, limeLight, sensors, 1500);
        Turn turnToTarget = new Turn(driveTrain, sensors, -29, 1500);
        TurnSpeedPos turnToTrench = new TurnSpeedPos(driveTrain, sensors,  120, -149, 1300);
        addCommands(
            new AutonFlyWheel(flyWheel, 0.7), // Set starting flywheel distance
            new ParallelCommandGroup(
                new SetHood(hood, Value.kForward, 500), // Actuate the Hood and intake pistons
                new SetIntakeActuator(intakeSystem, Value.kForward, 500)
             ),
            turnToTarget, //turns to face hole
            new ParallelCommandGroup(
                new AutonIndexer(indexer, 1500), //lets preloads into flywheel to shoot them
                new AutonIntake(intakeSystem, 1500, 0.475)
            ),
            new AutonFlyWheel(flyWheel, 0.72), // Adjusts for greater distance
            turnToTrench, //turn to face trench
            new ParallelCommandGroup( //drives forward into trench and intakes balls
                new DriveForwardSpeedPosGyro(driveTrain, sensors, 50 * Constants.IN_SEC_TO_TICS_TENTH, 180 * Constants.ENCODER_TICS_PER_INCH, 4000),
                new AutonIntake(intakeSystem, 4000, 0.65)
            ),
            new Delay(100),
            new ParallelCommandGroup( //drives backwards from trench to get into shooting position.
                new DriveForwardSpeedPosGyro(driveTrain, sensors, 50 * Constants.IN_SEC_TO_TICS_TENTH, -80 * Constants.ENCODER_TICS_PER_INCH, 2200),
                new AutonIntake(intakeSystem, 2200, 0.5)
            ),
            new TurnSpeedPos(driveTrain, sensors, 120, 160, 1400),
            new LimeLightTurn(driveTrain, limeLight, sensors, 500),
            new ParallelCommandGroup(
                new AutonIndexer(indexer, 5000), //shoot balls from trench
                new AutonIntake(intakeSystem, 5000, 0.5)
            )
        );
    }

    

}
