package frc.robot.commands.controllers;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ColorWheel;

public class ColorWheelController extends CommandBase{

    private final ColorWheel colorWheel;
    private final XboxController controller;
    private double speed;
    private boolean enabled = false;
    private String matchColor = "";

    public ColorWheelController(ColorWheel colorWheel, double speed, XboxController controller, String matchColor){
        this.colorWheel = colorWheel;
        this.speed = speed;
        this.controller = controller;
        this.matchColor = matchColor;
        addRequirements(colorWheel);
    }

    @Override
    public void initialize() {      
    }

    @Override
    public void execute(){
        if(controller.getPOV() > 60 && controller.getPOV() < 130){
            colorWheel.colorWheelMotor.set(speed);
        } else if(controller.getPOV() > 240 && controller.getPOV() < 300){
            colorWheel.colorWheelMotor.set(speed*-1);
        } else {
            colorWheel.colorWheelMotor.set(0);
        }
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}