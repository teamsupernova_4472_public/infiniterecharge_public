package frc.robot.commands.controllers;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpiutil.math.MathUtil;
import frc.robot.Constants;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.LimeLight;

public class FlyWheelController extends CommandBase{

    private final FlyWheel flyWheel;
    private final XboxController controllerD;
    private final XboxController controllerP;
    private double speed;
    private final LimeLight limelight;
    private boolean enabledPartner = false;

    public double findVelocity() {
        double area = limelight.getTargetAreaPercent();
        speed = 0.0134171*Math.pow(area, 4) - 0.113063415*Math.pow(area, 3) + 0.3796602996*Math.pow(area, 2) - 0.5619844*area + 0.99373322;
        speed*= Constants.MAX_FLY_WHEEL_SPEED;
        SmartDashboard.putNumber("Calculated FlyWheel Speed", speed);
        return speed;
    }

    public FlyWheelController(FlyWheel flyWheel, double speed, XboxController controllerD, XboxController controllerP, LimeLight limelight){
        this.flyWheel = flyWheel;
        this.speed = speed;
        this.limelight = limelight;
        this.controllerD = controllerD;
        this.controllerP = controllerP;
        addRequirements(flyWheel);
    }

    @Override
    public void initialize() {
        flyWheel.flyWheelMotor.selectProfileSlot(1, 0);
    }

    @Override
    public void end(boolean interrputed) {
        flyWheel.flyWheelMotor.set(0.0);
    }

    @Override
    public void execute() {
        boolean pressedPartner = controllerP.getAButtonPressed();  //changed to partner control
        boolean pressedDriver = controllerD.getAButton();
        if(pressedPartner) {
            enabledPartner = !enabledPartner;
        }
        if(controllerD.getXButton()) {
            flyWheel.flyWheelMotor.set(ControlMode.Velocity, -0.05*Constants.MAX_FLY_WHEEL_SPEED);
        }
        else if (pressedDriver) {
            flyWheel.flyWheelMotor.set(ControlMode.Velocity, findVelocity());
        }
        else if(enabledPartner){
            if(controllerP.getXButtonPressed()) {
                speed -= 0.01 * Constants.MAX_FLY_WHEEL_SPEED;
                speed = MathUtil.clamp(speed, 0.0, Constants.MAX_FLY_WHEEL_SPEED);
            }
    
            if(controllerP.getYButtonPressed()) {
                speed += 0.01 * Constants.MAX_FLY_WHEEL_SPEED;
                speed = MathUtil.clamp(speed, 0.0, Constants.MAX_FLY_WHEEL_SPEED);
            }
            flyWheel.flyWheelMotor.set(ControlMode.Velocity, speed);
        }
        else {
            flyWheel.flyWheelMotor.set(ControlMode.PercentOutput, 0);
        }
        SmartDashboard.putNumber("Percent output", speed/Constants.MAX_FLY_WHEEL_SPEED);
     }

    @Override
    public boolean isFinished() {
        return false;
    }
}
