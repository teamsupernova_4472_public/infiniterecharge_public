package frc.robot.commands.controllers;

import edu.wpi.first.wpilibj.SlewRateLimiter;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotUtils;
import frc.robot.subsystems.Lift;

public class LiftController extends CommandBase
{
    private static final double LIFT_HEIGHT = -145;
    private static final double COLOR_WHEEL_HEIGHT = -28;
    private final Lift lift;
    private final XboxController controller;
    private final SlewRateLimiter limiter;
    private double minEnc;
    private Value latchState;


    public LiftController(Lift lift, XboxController controller)
    {
        latchState = Value.kForward;
        this.lift = lift;
        this.controller = controller;
        limiter = new SlewRateLimiter(1.0);
        addRequirements(lift);
    }

    @Override
    public void initialize() {
        minEnc = lift.liftMotor.getEncoder().getPosition();
        lift.latch.set(Value.kForward);
    }

    @Override
    public void end(boolean interrupted) {
        lift.liftMotor.set(0);
        lift.roller.set(0);
    }

    @Override
    public void execute()
    {
        double currentEncoderReading = lift.liftMotor.getEncoder().getPosition();
        double setVal = RobotUtils.deadband(0.1, controller.getY(Hand.kRight));
        double currentHeight = currentEncoderReading - minEnc;
        if(controller.getBumperPressed(Hand.kRight)){
            if(latchState == Value.kForward)
                latchState = Value.kReverse;
            else
                latchState = Value.kForward;
        }

        if(latchState == Value.kForward){
            SmartDashboard.putNumber("Climb Height", currentHeight);
            if((currentHeight > LIFT_HEIGHT && setVal < 0 )|| (currentHeight < 0 && setVal > 0)) {
                setVal = limiter.calculate(setVal);
            } else {
                setVal = 0;
            }
            lift.liftMotor.set(setVal);
            lift.roller.set((controller.getX(Hand.kLeft))/1.5);

            if(controller.getPOV() > 330 && controller.getPOV() < 30){
                setVal = -0.25;
                if((currentHeight > COLOR_WHEEL_HEIGHT && setVal < 0 )|| (currentHeight < 0 && setVal > 0)) {
                    setVal = limiter.calculate(setVal);
                } else {
                    setVal = 0;
                }
    
                lift.liftMotor.set(setVal);
            }
            if(controller.getPOV() > 150 && controller.getPOV() < 210){
                setVal = 0.25;
                if((currentHeight > COLOR_WHEEL_HEIGHT && setVal < 0 )|| (currentHeight < 0 && setVal > 0)) {
                    setVal = limiter.calculate(setVal);
                } else {
                    setVal = 0;
                }
                lift.liftMotor.set(setVal);
            }
        } else {
            lift.liftMotor.set(0);
        }
        
        lift.latch.set(latchState);
        SmartDashboard.putBoolean("Latch active", latchState == Value.kReverse);
        SmartDashboard.putNumber("Climb motor out", setVal);
    }

    @Override
    public boolean isFinished()
    {
        return false;
    }
}
