package frc.robot.commands.controllers;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.PneumaticHood;

public class PneumaticHoodController extends CommandBase
{
    PneumaticHood ph;
    XboxController controller;

    public PneumaticHoodController(PneumaticHood ph, XboxController controller)
    {
        this.ph = ph;
        this.controller = controller;
        addRequirements(ph);
    }

    @Override
    public void execute()
    {
        if(controller.getBButtonPressed())
        {
            if(ph.hoodPiston.get() == Value.kForward)
            {
                ph.hoodPiston.set(Value.kReverse);
            }
            else
            {
                ph.hoodPiston.set(Value.kForward);
            }
        }
    }
}