package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ColorWheel extends SubsystemBase {

    public final WPI_TalonSRX colorWheelMotor;
    public static String gameData;

    public ColorWheel(WPI_TalonSRX colorWheelMotor) {
        this.colorWheelMotor = colorWheelMotor;
        gameData = DriverStation.getInstance().getGameSpecificMessage();
        if(gameData.length() > 0){
            switch (gameData.charAt(0)){
                case 'B':

                    break;
                case 'R':

                    break;
                case 'Y':

                    break;
                case 'G':
                
                    break;
                default:

                    break;
            }
        }
        else{

        }
    }

    public static String getColor(){
        return gameData;
    }
}
