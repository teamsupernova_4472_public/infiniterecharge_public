package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;


public class DriveTrain extends SubsystemBase {    
    
  public final WPI_TalonSRX left;
  public final WPI_TalonSRX right;
  public final SpeedController gyroController;


  public DriveTrain(WPI_TalonSRX left, WPI_TalonSRX right){
    this.left = left;
    this.right = right;
    gyroController = new SpeedControllerGroup(left, right);
  }
  public void drive(double leftSpeed, double rightSpeed) {
      //Driver Control
    left.set(leftSpeed);
    right.set(rightSpeed);
    
  }

  public void autonDriveForward(double distance) {
    left.selectProfileSlot(0, 0);
    right.selectProfileSlot(0, 0);
    left.set(ControlMode.Position, -distance);
    right.set(ControlMode.Position, -distance);
  }

  public void driveSpeed(double leftSpeed, double rightSpeed) {
    setSpeed(left, leftSpeed);
    setSpeed(right,rightSpeed);
  }

  private void setSpeed(TalonSRX motor, double speed) {
    left.selectProfileSlot(1, 0);
    right.selectProfileSlot(1, 0);
    double val = speed;
    if(Math.abs(speed) < 0.1) {
      val = 0;
    }

    if(val == 0) {
      motor.set(ControlMode.PercentOutput, 0.0);
    } else {
      motor.set(ControlMode.Velocity, val * Constants.MAX_DRIVE_SPEED);
    } 
  }

  public void autonDriveForwardSpeed(double speed) {
    autonDriveForwardSpeed(speed, speed);
  }

  public void autonDriveForwardSpeed(double leftSpeed, double rightSpeed) {
    left.selectProfileSlot(1, 0);
    right.selectProfileSlot(1, 0);
    left.set(ControlMode.Velocity, -leftSpeed);
    right.set(ControlMode.Velocity, -rightSpeed);
  }

  public void turn(double percent) {
    left.selectProfileSlot(0, 0);
    right.selectProfileSlot(0, 0);
    left.set(-percent);
    right.set(percent);
  }

  public void turnSpeed(double speed) {
    left.selectProfileSlot(1, 0);
    right.selectProfileSlot(1, 0);
    left.set(ControlMode.Velocity, -speed);
    right.set(ControlMode.Velocity, speed);
  }

  public double getPosition() {
    double leftPos = -left.getSensorCollection().getQuadraturePosition();
    double rightPos = right.getSensorCollection().getQuadraturePosition();
    return (leftPos + rightPos) / 2;
  }

}
