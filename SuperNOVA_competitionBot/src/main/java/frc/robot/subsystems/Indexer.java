package frc.robot.subsystems;

import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;


public class Indexer extends SubsystemBase {    
    
  public final SpeedController speedController;


  public Indexer(SpeedController speedController){
    this.speedController = speedController;
  }

}
