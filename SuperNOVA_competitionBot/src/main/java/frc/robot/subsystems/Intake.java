package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Intake extends SubsystemBase{
    public final SpeedController intake;
    public final DoubleSolenoid intakeActuator;

    public Intake(SpeedController intake, DoubleSolenoid intakeActuator){
        this.intake = intake;
        this.intakeActuator = intakeActuator;
    }

}