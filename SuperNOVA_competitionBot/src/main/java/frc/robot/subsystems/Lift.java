package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Lift extends SubsystemBase
{
    public final CANSparkMax liftMotor;
    public final SpeedController roller;
    public final DoubleSolenoid latch;

    public Lift(CANSparkMax lift, SpeedController roller, DoubleSolenoid latch)
    {
        this.liftMotor = lift;
        this.roller = roller;
        this.latch = latch;
    }
}