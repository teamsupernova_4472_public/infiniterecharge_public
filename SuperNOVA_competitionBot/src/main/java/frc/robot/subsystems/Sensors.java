/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/
package frc.robot.subsystems;
import com.ctre.phoenix.motorcontrol.SensorCollection;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;
import com.revrobotics.CANSparkMax;

import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Sensors extends SubsystemBase {
    private final WPI_TalonSRX leftEncoder;
    private final WPI_TalonSRX rightEncoder;
    private final WPI_TalonSRX flyWheel;
    private final CANSparkMax sparkMax;
    public final AHRS gyro;
    private final PowerDistributionPanel pdp;
    private ColorWheel colorWheel;

    public Sensors(WPI_TalonSRX leftEncoder, WPI_TalonSRX rightEncoder, 
                   WPI_TalonSRX flyWheel, CANSparkMax sparkMax, AHRS gyro, PowerDistributionPanel pdp, ColorWheel colorWheel) {
        this.leftEncoder = leftEncoder;
        this.rightEncoder = rightEncoder;
        this.flyWheel = flyWheel;
        this.gyro = gyro;
        this.sparkMax = sparkMax;
        this.pdp = pdp;
        this.colorWheel = colorWheel;
    }

    public void outputSensors() {
        SensorCollection leftSensors = getLeftSensors();
        SensorCollection rightSensors = getRightSensors();
        SensorCollection flyWheelSensors = getFlyWheelSensors();
        int leftEncoderTics = leftSensors.getQuadraturePosition();
        int rightEncoderTics = rightSensors.getQuadraturePosition();
        int leftSpeed = leftSensors.getQuadratureVelocity();
        int rightSpeed = rightSensors.getQuadratureVelocity();
        int flyWheelSpeed = flyWheelSensors.getQuadratureVelocity();
        int flyWheelPosition = flyWheelSensors.getQuadraturePosition();
        double rollVal = gyro.getRoll();
        double pitchVal = gyro.getPitch();
        SmartDashboard.putNumber("Left encoder tics", leftEncoderTics);
        SmartDashboard.putNumber("Right encoder tics", rightEncoderTics);
        SmartDashboard.putNumber("Left speed (tics per 100ms)", leftSpeed);
        SmartDashboard.putNumber("Right speed (tics per 100ms)", rightSpeed);
        SmartDashboard.putNumber("Fly wheel speed", flyWheelSpeed);
        SmartDashboard.putNumber("Fly wheel position", flyWheelPosition);
        SmartDashboard.putNumber("Elevator Position", sparkMax.getEncoder().getPosition());
        SmartDashboard.putData("Yaw", gyro);
        SmartDashboard.putNumber("Yaw Rate", getYawRate());
        SmartDashboard.putNumber("Roll", rollVal);
        SmartDashboard.putNumber("Pitch", pitchVal);
        SmartDashboard.putData("Power Distribution Panel", pdp);
        SmartDashboard.putString("Selected Color", ColorWheel.getColor());
    }

    public SensorCollection getLeftSensors() {
        return leftEncoder.getSensorCollection();
    }

    public SensorCollection getRightSensors() {
        return rightEncoder.getSensorCollection();
    }

    public SensorCollection getFlyWheelSensors() {
        return flyWheel.getSensorCollection();
    }

    /**
     * Get method for retrieving the yaw from the Gyro.
     * 
     * @return The yaw rate in degrees pers second.
     */
    public double getYawRate() {
        // getRate has inaccurate documentatation. The accurate yaw rate measurment
        // is determined from getRate() * getAcutalUpdateRate()
        double yawRate = 0;
        try {
            yawRate =gyro.getRate() * gyro.getActualUpdateRate();
        } catch(ArithmeticException e) {
            System.out.println("Yaw rate calculations has divided by 0" + e.getMessage());
        }

        return yawRate;
    }
}