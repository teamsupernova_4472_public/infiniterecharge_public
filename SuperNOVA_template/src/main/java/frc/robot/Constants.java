/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    /**
     * Gyro PID constants.
     */
    public static final double GYRO_P = 0.019;
    public static final double GYRO_I = 0.0;
    public static final double GYRO_D = 0.002;
    public static final double GYRO_SPEED_P = 0.005;
    public static final double GYRO_SPEED_I = 0.0;
    public static final double GYRO_SPEED_D = 0.00001;
    public static final double MAX_DRIVE_SPEED = 800;
    public static final int MAX_FLY_WHEEL_SPEED = 90000;
    public static final double DISTANCE_K = 1;
    public static final double HEIGHT_OF_GOAL = 8.1875;
    public static final double LIMELIGHT_MOUNTING_HEIGHT = 1.5;
    public static final double GRAVITY = 32;
    public static final double HOOD_DOWN_ANGLE = 30; //measure this on bot later
    public static final double HOOD_UP_ANGLE = 60; //measure this on bot later
    public static final double ENCODER_TICS_PER_REVOLUTION = 1441;
    public static final double WHEEL_CIRCUMFERENCE = 6.0 * Math.PI;
    public static final double ENCODER_TICS_PER_INCH = ENCODER_TICS_PER_REVOLUTION / WHEEL_CIRCUMFERENCE;
    public static final double IN_SEC_TO_TICS_TENTH = ENCODER_TICS_PER_INCH / 10;
    public static final double ENCODER_TICS_PER_FLYWHEEL_REVOLUTION = 12272;
}
