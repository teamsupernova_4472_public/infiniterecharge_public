/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import java.util.HashMap;
import java.util.Map;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.commands.AutonFlyWheel;
import frc.robot.commands.AutonIndexer;
import frc.robot.commands.AutonIntake;
import frc.robot.commands.TargetTrenchAutoPath;
import frc.robot.commands.Distance1;
import frc.robot.commands.Distance2;
import frc.robot.commands.DriveForward;
import frc.robot.commands.DriveForwardSpeed;
import frc.robot.commands.DriveForwardSpeedPos;
import frc.robot.commands.DriveTrainController;
import frc.robot.commands.FlyWheelController;
import frc.robot.commands.IndexerController;
import frc.robot.commands.IntakeController;
import frc.robot.commands.LiftController;
import frc.robot.commands.LimeLightTurn;
import frc.robot.commands.LowerRendezvousPath;
import frc.robot.commands.PneumaticHoodController;
import frc.robot.commands.Turn;
import frc.robot.commands.TurnSpeed;
import frc.robot.commands.TurnSpeedPos;
import frc.robot.commands.UpperRendezvousPath;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Lift;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.PneumaticHood;
import frc.robot.subsystems.Sensors;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  // Sensors
  private final WPI_TalonSRX leftEncoder = new WPI_TalonSRX(2);
  private final WPI_VictorSPX left = new WPI_VictorSPX(3);
  private final WPI_TalonSRX rightEncoder = new WPI_TalonSRX(4);
  private final WPI_VictorSPX right = new WPI_VictorSPX(5);
  private final DoubleSolenoid intakeActuator = new DoubleSolenoid(0, 2, 3);
  private final AHRS gyro = new AHRS();

  // Controllers
  private final XboxController driver = new XboxController(0);
  private final XboxController partner = new XboxController(1);

  // Subsystems
  private final Sensors sensors = new Sensors(leftEncoder, rightEncoder, leftEncoder, gyro);
  private final LimeLight limeLight = new LimeLight();
  private final DriveTrain driveTrain = new DriveTrain(leftEncoder, rightEncoder);
  private final FlyWheel flyWheel = new FlyWheel(new WPI_TalonSRX(6));
  private final Indexer indexer = new Indexer(new WPI_TalonSRX(7));
  private final Intake intake = new Intake(new WPI_TalonSRX(8), intakeActuator);
  private final Lift lift = new Lift(new WPI_TalonSRX(9), new WPI_TalonSRX(10));
  private final PneumaticHood ph = new PneumaticHood(new DoubleSolenoid(0, 0, 1));

  // Commands
  final Command autoCommand = new UpperRendezvousPath(driveTrain, sensors, limeLight, flyWheel, indexer, intake);
  final TurnSpeed turnSpeed = new TurnSpeed(driveTrain, sensors, 120, 50000);
  final Turn turn;
  final TurnSpeedPos turnSpeedPos = new TurnSpeedPos(driveTrain, sensors, 120, -180, 50000);
  final DriveForwardSpeed driveForwardSpeed = new DriveForwardSpeed(driveTrain, 300, 1000);
  final DriveForward driveForward = new DriveForward(driveTrain, 5000, 50000);
  final DriveForwardSpeedPos driveForwardSpeedPos = new DriveForwardSpeedPos(driveTrain, 400, 5000, 50000);
  final LimeLightTurn limeLightTurn = new LimeLightTurn(driveTrain, limeLight, 2000);
  private final DriveTrainController drive = new DriveTrainController(driveTrain, driver);
  private final IndexerController indexerController = new IndexerController(indexer, driver);
  private final IntakeController intakeController = new IntakeController(driver, intake, 0.5);
  private final LiftController liftController = new LiftController(lift, partner);
  private final PneumaticHoodController hoodController = new PneumaticHoodController(ph, driver);
  private final FlyWheelController flyWheelController = new FlyWheelController(flyWheel, Constants.MAX_FLY_WHEEL_SPEED, driver, hoodController);
  private final Distance1 distance1 = new Distance1(limeLight);
  private final Distance2 distance2 = new Distance2(limeLight);
  private final AutonFlyWheel autonFlyWheel = new AutonFlyWheel(flyWheel, 4*Constants.MAX_FLY_WHEEL_SPEED);
  private final AutonIndexer autonIndexer = new AutonIndexer(indexer, 1000);
  private final AutonIntake autonIntake = new AutonIntake(intake, 1000);
  private final SendableChooser<Command> chooser;
  private final LowerRendezvousPath autopath = new LowerRendezvousPath(driveTrain, sensors, limeLight, flyWheel, indexer, intake);

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    Map<String, NetworkTableEntry> gyroTuningTab = createGyroTuningTab();
    NetworkTableEntry pEntry = gyroTuningTab.get("P");
    NetworkTableEntry iEntry = gyroTuningTab.get("I");
    NetworkTableEntry dEntry = gyroTuningTab.get("D");
    turn = new Turn(driveTrain, sensors, -90, 500000, pEntry, iEntry, dEntry);
    chooser = new SendableChooser<Command>();
    left.follow(leftEncoder);
    right.follow(rightEncoder);
    leftEncoder.setSensorPhase(true);
    rightEncoder.setSensorPhase(true);
    CommandScheduler.getInstance().setDefaultCommand(driveTrain, drive);
    CommandScheduler.getInstance().setDefaultCommand(flyWheel, flyWheelController);
    CommandScheduler.getInstance().setDefaultCommand(indexer, indexerController);
    CommandScheduler.getInstance().setDefaultCommand(intake, intakeController);
    CommandScheduler.getInstance().setDefaultCommand(lift, liftController);
    CommandScheduler.getInstance().setDefaultCommand(ph, hoodController);
    //TODO: Color Picker (EVERYTHING)
    chooser.setDefaultOption("Trench Auton", autoCommand);
    chooser.addOption("Upper Rendevous", turnSpeed);
    chooser.addOption("Lower Rendevous", driveForwardSpeed);
    chooser.addOption("Lime light turn", limeLightTurn);
    Shuffleboard.getTab("Auton Select").add("Auton Selection", chooser)
        .withWidget(BuiltInWidgets.kSplitButtonChooser);
    // Configure the button bindings
    configureButtonBindings();
  }

  private static Map<String, NetworkTableEntry> createGyroTuningTab() {
    Map<String, NetworkTableEntry> tuningEntries = new HashMap<>();
    ShuffleboardTab gyroTuningTab = Shuffleboard.getTab("Gyro Tuning");
    NetworkTableEntry pEntry = gyroTuningTab.add("P", Constants.GYRO_P).getEntry();
    NetworkTableEntry iEntry = gyroTuningTab.add("I", Constants.GYRO_I).getEntry();
    NetworkTableEntry dEntry = gyroTuningTab.add("D", Constants.GYRO_D).getEntry();
    tuningEntries.put("P", pEntry);
    tuningEntries.put("I", iEntry);
    tuningEntries.put("D", dEntry);
    return tuningEntries;
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
  }


  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    //return chooser.getSelected();
    return limeLightTurn;
  }

  public void outputSensors() {
    sensors.outputSensors();
  }
}
