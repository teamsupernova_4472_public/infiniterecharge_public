package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Indexer;

public class AutonIndexer extends CommandBase{

    private final Indexer indexer;
    private final Timer timer = new Timer();
    private final int timeout;

    public AutonIndexer(Indexer indexer, int timeout){
        this.indexer = indexer;
        this.timeout = timeout;
        addRequirements(indexer);
    }

    @Override
    public void initialize() {
        timer.reset();
        timer.start();
    }

    @Override
    public void end(boolean interrputed) {
        timer.stop();
        indexer.speedController.set(0.0);
    }

    @Override
    public void execute() {
        indexer.speedController.set(1.0);
     }

    @Override
    public boolean isFinished() {
        boolean isFinished = timeout < timer.get() * 1000;
        return isFinished;
    }
}