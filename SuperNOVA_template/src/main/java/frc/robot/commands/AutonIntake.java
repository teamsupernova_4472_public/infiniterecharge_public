package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Intake;

public class AutonIntake extends CommandBase {
    private final Intake intakeSystem;
    private final Timer timer = new Timer();
    private final int timeout;

    public AutonIntake(Intake intakeSystem, int timeout) {
        this.intakeSystem = intakeSystem;
        this.timeout = timeout;
        addRequirements(intakeSystem);
    }
    @Override
    public void initialize() {
        timer.reset();
        timer.start();
    }
    @Override
    public void execute() {
        intakeSystem.intake.set(1);
    }
    @Override
    public void end(boolean interrputed) {
        timer.stop();
        intakeSystem.intake.set(0);
    }
    @Override
    public boolean isFinished() {
        boolean isFinished = timeout < timer.get() * 1000;
        return isFinished;
    }
}