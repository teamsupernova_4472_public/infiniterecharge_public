package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import java.lang.Math;
import frc.robot.Constants;
import frc.robot.subsystems.LimeLight;

public class Distance1 extends CommandBase {
    private LimeLight limeLight = new LimeLight();

    public Distance1(LimeLight limeLight) {
        this.limeLight = limeLight;
    }

    @Override
    public void execute() {
        double area = limeLight.getTargetAreaPercent();
        double distance = Constants.DISTANCE_K/Math.sqrt(area);
        SmartDashboard.putNumber("Distance to goal", distance);
    }
}