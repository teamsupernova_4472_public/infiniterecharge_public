package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import java.lang.Math;
import frc.robot.Constants;
import frc.robot.subsystems.LimeLight;

public class Distance2 extends CommandBase {
    private LimeLight limeLight = new LimeLight();

    public Distance2(LimeLight limeLight) {
        this.limeLight = limeLight;
    }

    @Override
    public void execute() {
        double y = limeLight.getYDegreesAwayFromTarget();
        double distance = (Constants.HEIGHT_OF_GOAL-Constants.LIMELIGHT_MOUNTING_HEIGHT) / Math.tan(y);
        SmartDashboard.putNumber("Distance to goal", distance);
    }
}

