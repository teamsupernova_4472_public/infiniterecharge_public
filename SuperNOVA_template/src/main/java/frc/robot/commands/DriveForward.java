
package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class DriveForward extends CommandBase {
    private final DriveTrain drive;
    private final int distance;
    private final int timeOut;
    private final Timer timer =  new Timer();
    public DriveForward(DriveTrain drive, int distance, int timeOut) {
        this.drive = drive;
        this.distance = distance;
        this.timeOut = timeOut;
        addRequirements(drive);        
    }

    @Override
    public void initialize(){
       timer.reset();
       timer.start();
       drive.left.selectProfileSlot(0, 0);
       drive.right.selectProfileSlot(0, 0);
       drive.left.getSensorCollection().setQuadraturePosition(0, 100);
       drive.right.getSensorCollection().setQuadraturePosition(0, 100);
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        drive.drive(0.0, 0.0);
    }

    @Override
    public void execute() {
        drive.autonDriveForward(distance);
    }

    @Override
    public boolean isFinished() {
      double currentTimeMS = timer.get()*1000;
      boolean isFinished = currentTimeMS > timeOut;
      return isFinished;
    }
}