
package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class DriveForwardSpeedPos extends CommandBase {
    private static final double STOP_PERCENT = 0.9;
    private final DriveTrain drive;
    private final double speed;
    private final double position;
    private final int timeOut;
    private final Timer timer =  new Timer();

    /**
     * Contructor
     * 
     * @param drive The drive train sub system
     * @param speed The speed to drive the robot.
     * @param position The position for the robot to reach.
     * @param timeOut the amount of time give to drive to target.
     */
    public DriveForwardSpeedPos(DriveTrain drive, double speed, double position, int timeOut) {
        this.drive = drive;
        this.position = position;
        this.speed = position > 0 ? Math.abs(speed) : -Math.abs(speed);
        this.timeOut = timeOut;
        addRequirements(drive);        
    }

    @Override
    public void initialize(){
       timer.reset();
       timer.start();
       drive.left.getSensorCollection().setQuadraturePosition(0, 100);
       drive.right.getSensorCollection().setQuadraturePosition(0, 100);
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        drive.drive(0.0, 0.0);
    }

    @Override
    public void execute() {
        int currentPosition = drive.getPosition();
        if((position > 0 && currentPosition < STOP_PERCENT * position) ||
            (position < 0 && currentPosition > STOP_PERCENT * position)) {
            drive.autonDriveForwardSpeed(speed);
        } else {
            drive.autonDriveForward(position);
        }
    }

    @Override
    public boolean isFinished() {
      double currentTimeMS = timer.get()*1000;
      boolean isFinished = currentTimeMS > timeOut;
      return isFinished;
    }
}