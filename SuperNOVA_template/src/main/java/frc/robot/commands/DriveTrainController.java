/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.SlewRateLimiter;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

/**
 * An example command.  You can replace me with your own command.
 */
public class DriveTrainController extends CommandBase {
  
  private final DriveTrain driveTrain;
  private final XboxController controller;
  private final SlewRateLimiter leftLimiter;
  private final SlewRateLimiter rightLimiter;

  public DriveTrainController(DriveTrain driveTrain, XboxController controller) {
    this.driveTrain = driveTrain;
    this.controller = controller;
    leftLimiter = new SlewRateLimiter(1.0);
    rightLimiter = new SlewRateLimiter(1.0);
    addRequirements(driveTrain);
  }

  // Called just before this Command runs the first time
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double leftStick = -controller.getY(Hand.kLeft);
    double rightStick = controller.getY(Hand.kRight);
    double leftSpeed = leftStick > 0 ? leftStick*leftStick : -1 * leftStick *leftStick;
    double rightSpeed = rightStick > 0 ? rightStick*rightStick : -1 * rightStick * rightStick;
    
    driveTrain.driveSpeed(leftLimiter.calculate(leftStick), rightLimiter.calculate(rightStick));
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveTrain.drive(0.0, 0.0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}