package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.LimeLight;
import frc.robot.Constants;

public class FlyWheelController extends CommandBase{

    private final FlyWheel flyWheel;
    private final XboxController controller;
    private final PneumaticHoodController hoodController;
    private double speed;
    private LimeLight limelight;
    private boolean enabled = false;

    public double getDistance1() {
        double area = limelight.getTargetAreaPercent();
        double distance1 = Constants.DISTANCE_K/Math.sqrt(area);
        return distance1;
    }

    public double getDistance2() {
        double y = limelight.getYDegreesAwayFromTarget();
        double distance2 = (Constants.HEIGHT_OF_GOAL-Constants.LIMELIGHT_MOUNTING_HEIGHT) / Math.tan(y);
        return distance2;
    }

    public double findInitialVelocity() {
        double hoodAngle = (hoodController.ph.hoodPiston.get() == Value.kForward) ? Constants.HOOD_UP_ANGLE : Constants.HOOD_DOWN_ANGLE;
        speed = Math.sqrt(Constants.GRAVITY * Math.pow(getDistance1(), 2)
                / (2 * Math.pow(Math.cos(hoodAngle), 2)
                        * (Math.tan(hoodAngle) * getDistance1() - Constants.HEIGHT_OF_GOAL
                                - Constants.LIMELIGHT_MOUNTING_HEIGHT)));
        speed = speed * (12 * Constants.ENCODER_TICS_PER_FLYWHEEL_REVOLUTION) / (Math.PI*4);
        return speed;
    }

    public FlyWheelController(FlyWheel flyWheel, double speed, XboxController controller, PneumaticHoodController hoodController){
        this.flyWheel = flyWheel;
        this.speed = speed;
        this.controller = controller;
        this.hoodController = hoodController;
        addRequirements(flyWheel);
    }

    public FlyWheelController(FlyWheel flyWheel, XboxController controller, PneumaticHoodController hoodController){
        this.flyWheel = flyWheel;
        this.controller = controller;
        this.hoodController = hoodController;
        addRequirements(flyWheel);
    }

    @Override
    public void initialize() {
        flyWheel.flyWheelMotor.selectProfileSlot(1, 0);
    }

    @Override
    public void end(boolean interrputed) {
        flyWheel.flyWheelMotor.set(0.0);
    }

    @Override
    public void execute() {
        boolean pressed = controller.getAButton();
        if(pressed) {
            enabled = !enabled;
        }
        if(enabled){
            if(getDistance1() > 60) { //hood up
                hoodController.ph.hoodPiston.set(Value.kReverse);
                flyWheel.flyWheelMotor.set(ControlMode.Velocity, findInitialVelocity());
            }
            else if(getDistance1() < 60) { //hood down
                hoodController.ph.hoodPiston.set(Value.kForward);
                flyWheel.flyWheelMotor.set(ControlMode.Velocity, findInitialVelocity());
            }
        } 
        else {
            flyWheel.flyWheelMotor.set(ControlMode.PercentOutput, 0);
        }
     }

    @Override
    public boolean isFinished() {
        return false;
    }
}