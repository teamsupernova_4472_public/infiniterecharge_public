package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.FlywheelHood;
import frc.robot.Constants;
import frc.robot.subsystems.LimeLight;


public class HoodChanger extends CommandBase{
    private LimeLight limelight;
    private final FlywheelHood flywheelHood;
    private double angle;

    public HoodChanger(FlywheelHood flywheelHood, double angle, LimeLight limelight){
        this.flywheelHood = flywheelHood;
        this.angle = angle;
        this.limelight = limelight;
    }

    public double getDistance1() {
        double area = limelight.getTargetAreaPercent();
        double distance1 = Constants.DISTANCE_K/Math.sqrt(area);
        return distance1;
    }

    public double getDistance2() {
        double y = limelight.getYDegreesAwayFromTarget();
        double distance2 = (Constants.HEIGHT_OF_GOAL-Constants.LIMELIGHT_MOUNTING_HEIGHT) / Math.tan(y);
        return distance2;
    }

    @Override
    public void initialize() {
        double x_method_1 = getDistance1();
        double x_method_2 = getDistance2();
        double y = Constants.HEIGHT_OF_GOAL-Constants.LIMELIGHT_MOUNTING_HEIGHT;
        double g = 9.81;
        double v_initial = 4*Constants.MAX_FLY_WHEEL_SPEED;
        // For quadratic solving
        double a = (g*x_method_1)/(Math.pow(v_initial,2));
        double b = -x_method_1;
        double c = ((g*x_method_1)/(Math.pow(v_initial,2))) - y;

        double determinant = (b * b) - (4 * a * c);

        if(determinant > 0) {
            double angle1 = Math.atan2((-b + Math.sqrt(determinant)), (2 * a)); //atan2 allows for divisions by 0 to be 0 instead of NaN (a.k.a run-time error)
            double angle2 = Math.atan2((-b - Math.sqrt(determinant)), (2 * a)); //Please use atan2 from now on if needed  --Evan
        }
        else {
            double angle = Math.atan2(-b, (2 * a));
        }

        // Incomplete, need to move hood to reflect the angle needed for scoring

    }

    @Override
    public void end(boolean interrputed) {
    }

    @Override
    public void execute() {
     }

    @Override
    public boolean isFinished() {
        return false;
    }
}