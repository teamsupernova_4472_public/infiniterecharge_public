/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Indexer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;

/**
 * An example command.  You can replace me with your own command.
 */
public class IndexerController extends CommandBase {
  private final XboxController controller;
  private final Indexer indexer;

  public IndexerController(Indexer indexer, XboxController controller) {
    this.indexer = indexer;
    this.controller = controller;
    addRequirements(indexer);
  }

  @Override
  public void end(boolean interrupted) {
      indexer.speedController.set(0);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
      if(controller.getBumper(Hand.kRight)){
        indexer.speedController.set(1.0);
      }
      else{
        indexer.speedController.set(0);
      }

  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}