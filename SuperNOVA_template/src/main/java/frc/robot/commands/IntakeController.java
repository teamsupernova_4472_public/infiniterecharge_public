package frc.robot.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Intake;
public class IntakeController extends CommandBase {
    private final XboxController controller;
    private final Intake intakeSystem;
    private final double speed;
    public IntakeController(XboxController controller, Intake intakeSystem, double speed) {
        this.controller = controller;
        this.intakeSystem = intakeSystem;
        this.speed = speed;
        addRequirements(intakeSystem);
    }

    @Override
    public void execute() {
        if(controller.getBumperPressed(Hand.kLeft)){
            Value intakeVal = intakeSystem.intakeActuator.get();
            SmartDashboard.putString("Intake Value", intakeVal.name());
            if(intakeVal == Value.kForward) {
                intakeVal = Value.kReverse;
            } else {
                intakeVal = Value.kForward;
            }
            intakeSystem.intakeActuator.set(intakeVal);
        }
        else{
            intakeSystem.intake.set(-controller.getTriggerAxis(Hand.kLeft));
        }
     }

    @Override
    public void end(boolean interrupted) {
        intakeSystem.intake.set(0);
        intakeSystem.intakeActuator.set(Value.kOff);
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}