package frc.robot.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Lift;

public class LiftController extends CommandBase
{
    Lift lift;
    XboxController controller;

    public LiftController(Lift lift, XboxController controller)
    {
        this.lift = lift;
        this.controller = controller;
        addRequirements(lift);
    }

    @Override
    public void end(boolean interrupted) {
        lift.liftMotor.set(0);
        lift.roller.set(0);
    }

    @Override
    public void execute()
    {
        lift.liftMotor.set(controller.getY(Hand.kRight));
        lift.roller.set(controller.getX(Hand.kLeft));
    }

    @Override
    public boolean isFinished()
    {
        return false;
    }
}
