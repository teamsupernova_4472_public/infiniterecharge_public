
package frc.robot.commands;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import edu.wpi.first.wpiutil.math.MathUtil;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.LimeLight;


public class LimeLightTurn extends PIDCommand{
    private final Timer timer = new Timer();
    private final int timeout;
    private final PIDController controller;
    private final NetworkTableEntry pEntry;
    private final NetworkTableEntry iEntry;
    private final NetworkTableEntry dEntry;
    private final DriveTrain driveTrain;
    private final LimeLight limeLight;

    public LimeLightTurn(DriveTrain driveTrain, LimeLight limeLight, int timeout) {
        this(driveTrain, limeLight, timeout, null, null, null);
    }

    
    public LimeLightTurn(DriveTrain driveTrain, LimeLight limeLight,  int timeout,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        this(driveTrain, limeLight, timeout, new PIDController(Constants.GYRO_P, 0.01, Constants.GYRO_D), 
                pEntry, iEntry, dEntry);
    }

    private LimeLightTurn(DriveTrain driveTrain, LimeLight limeLight, int timeout, PIDController controller,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        super(controller, //<-- PID Controller
            limeLight::getXDegreesAwayFromTarget, // <-- access to gyro angle method
            0, //angle you want to go to
            output -> {
                double degFromTarge = limeLight.getXDegreesAwayFromTarget();
                SmartDashboard.putNumber("Degrees From Targe", degFromTarge);
                double clamped = -MathUtil.clamp(output, -1, 1);
                SmartDashboard.putNumber("Gyro turn output", clamped);
                driveTrain.left.set(clamped);
                driveTrain.right.set(clamped);
            }, //<--output command on robot
            driveTrain, //drive train requirement
            limeLight//sensor requirement
            );
        this.timeout = timeout;
        this.controller = controller;
        this.pEntry = pEntry;
        this.iEntry = iEntry;
        this.dEntry = dEntry;
        this.driveTrain = driveTrain;
        this.limeLight = limeLight;
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
        controller.reset();
        driveTrain.left.selectProfileSlot(1, 0);
        driveTrain.right.selectProfileSlot(1, 0);
        if(pEntry != null && iEntry != null && dEntry != null) {
            controller.setP(pEntry.getDouble(0));
            controller.setI(iEntry.getDouble(0));
            controller.setD(dEntry.getDouble(0));
        }
        limeLight.setLEDMode(0);
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        driveTrain.drive(0, 0);
        limeLight.setLEDMode(1);
    }

    @Override
    public boolean isFinished() {
        boolean isFinished = timeout < timer.get() * 1000;
        return isFinished;
    }
     
}