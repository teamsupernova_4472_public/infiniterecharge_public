package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.Sensors;
import frc.robot.Constants;

public class LowerRendezvousPath extends SequentialCommandGroup {


    public LowerRendezvousPath(DriveTrain drivetrain, Sensors sensors, LimeLight limelight, FlyWheel flyWheel, Indexer indexer, Intake intakeSystem) {
        addCommands(
            new AutonFlyWheel(flyWheel, 0.75),
            new LimeLightTurn(drivetrain, limelight, 750),
            new AutonIndexer(indexer, 1000),
            new TurnSpeedPos(drivetrain, sensors, 120, 145, 1750),
            new ParallelCommandGroup(
                new DriveForwardSpeedPos(drivetrain, 40 * Constants.IN_SEC_TO_TICS_TENTH, 94.0 * Constants.ENCODER_TICS_PER_INCH, 3050),
                new AutonIntake(intakeSystem, 2500)
            ),
            new DriveForwardSpeedPos(drivetrain, 40 * Constants.IN_SEC_TO_TICS_TENTH, -20 * Constants.ENCODER_TICS_PER_INCH, 2020),
            new TurnSpeedPos(drivetrain, sensors, 120, -140, 2000),
            new LimeLightTurn(drivetrain, limelight, 3041),
            new AutonFlyWheel(flyWheel, 1),
            new AutonIndexer(indexer, 1000)
        );
    }

    

}