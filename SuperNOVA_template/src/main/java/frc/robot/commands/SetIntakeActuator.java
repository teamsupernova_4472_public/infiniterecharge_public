package frc.robot.commands;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Intake;

public class SetIntakeActuator extends CommandBase {
    public Intake intake;
    public Value intakeValue;

    public SetIntakeActuator(Intake intake, Value intakeValue) {
        this.intake = intake;
        this.intakeValue = intakeValue;
        addRequirements(intake);
    }

    @Override
    public void initialize() {
        intake.intakeActuator.set(intakeValue);
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}