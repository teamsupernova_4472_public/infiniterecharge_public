package frc.robot.commands;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.Sensors;

public class TargetTrenchAutoPath extends SequentialCommandGroup {


    public TargetTrenchAutoPath(DriveTrain driveTrain, Sensors sensors, LimeLight limeLight, FlyWheel flyWheel, Indexer indexer, Intake intakeSystem) {
        addCommands(
            new SetIntakeActuator(intakeSystem, Value.kForward),
            new AutonFlyWheel(flyWheel, 0.75),
            new LimeLightTurn(driveTrain, limeLight, 1000), //turns to face hole
            new AutonIndexer(indexer, 1000), //lets preloads into flywheel to shoot them
            new AutonFlyWheel(flyWheel, 0.85),
            new TurnSpeedPos(driveTrain, sensors, 120, 135, 1200), //turn to face trench
            new DriveForwardSpeedPos(driveTrain, 40 * Constants.IN_SEC_TO_TICS_TENTH, 115 * Constants.ENCODER_TICS_PER_INCH, 3000),
            new TurnSpeedPos(driveTrain, sensors, 120, 45, 400), //turn to face trench
            new ParallelCommandGroup( //drives forward into trench and intakes balls
                new DriveForwardSpeedPos(driveTrain, 40 * Constants.IN_SEC_TO_TICS_TENTH, -38 * Constants.ENCODER_TICS_PER_INCH, 1000), //TODO: greater magnitude
                new AutonIntake(intakeSystem, 1000)
            ),
            new DriveForwardSpeedPos(driveTrain, 40 * Constants.IN_SEC_TO_TICS_TENTH, 38 * Constants.ENCODER_TICS_PER_INCH, 1000), //TODO: greater magnitude
            new TurnSpeedPos(driveTrain, sensors, 120, 160, 1400),
            new LimeLightTurn(driveTrain, limeLight, 1000),
            new AutonIndexer(indexer, 1000) //shoot three balls from trench
        );
    }

    

}
