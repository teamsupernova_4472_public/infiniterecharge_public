
package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import edu.wpi.first.wpiutil.math.MathUtil;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Sensors;


public class TurnSpeed extends PIDCommand{
    private static final double FEEDFORWARD = 0.0045;
    private final Sensors sensor;
    private final Timer timer = new Timer();
    private final int timeout;
    private final PIDController controller;
    private final NetworkTableEntry pEntry;
    private final NetworkTableEntry iEntry;
    private final NetworkTableEntry dEntry;
    private final DriveTrain driveTrain;

    public TurnSpeed(DriveTrain driveTrain, Sensors sensor, double angularVelcoity, int timeout) {
        this(driveTrain, sensor, angularVelcoity, timeout, null, null, null);
    }

    
    public TurnSpeed(DriveTrain driveTrain, Sensors sensor, double angularVelocity, int timeout,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        this(driveTrain, sensor, angularVelocity, timeout, new PIDController(Constants.GYRO_SPEED_P, Constants.GYRO_SPEED_I, Constants.GYRO_SPEED_D), 
                pEntry, iEntry, dEntry);
    }

    private TurnSpeed(DriveTrain driveTrain, Sensors sensor, double angularVelocity, int timeout, PIDController controller,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        super(controller, //<-- PID Controller
            sensor::getYawRate, // <-- access to gyro angle method
            angularVelocity, //angle you want to go to
            output -> {
                double outputPlusFeedForward = angularVelocity * FEEDFORWARD;
                double clamped = MathUtil.clamp(outputPlusFeedForward, -1, 1);
                double speed = clamped * Constants.MAX_DRIVE_SPEED;
                SmartDashboard.putNumber("Gyro turn output", output);
                driveTrain.left.set(ControlMode.Velocity, speed);
                driveTrain.right.set(ControlMode.Velocity, speed);
            }, //<--output command on robot
            driveTrain, //drive train requirement
            sensor//sensor requirement
            );
        this.sensor = sensor;
        this.timeout = timeout;
        this.controller = controller;
        this.pEntry = pEntry;
        this.iEntry = iEntry;
        this.dEntry = dEntry;
        this.driveTrain = driveTrain;
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
        sensor.gyro.reset();
        controller.reset();
        driveTrain.left.selectProfileSlot(1, 0);
        driveTrain.right.selectProfileSlot(1, 0);
        if(pEntry != null && iEntry != null && dEntry != null) {
            controller.setP(pEntry.getDouble(0));
            controller.setI(iEntry.getDouble(0));
            controller.setD(dEntry.getDouble(0));
        }
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        driveTrain.drive(0.0, 0.0);
    }

    @Override
    public boolean isFinished() {
        boolean isFinished = timeout < timer.get() * 1000;
        return isFinished;
    }
     
}