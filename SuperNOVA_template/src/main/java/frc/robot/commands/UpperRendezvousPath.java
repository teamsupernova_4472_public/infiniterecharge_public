package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.FlyWheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.LimeLight;
import frc.robot.subsystems.Sensors;
import frc.robot.Constants;

public class UpperRendezvousPath extends SequentialCommandGroup {


    public UpperRendezvousPath(DriveTrain driveTrain, Sensors sensors, LimeLight limeLight, FlyWheel flyWheel, Indexer indexer, Intake intakeSystem) {

        addCommands(
                    new AutonFlyWheel(flyWheel, 4*Constants.MAX_FLY_WHEEL_SPEED),
                        new LimeLightTurn(driveTrain, limeLight, 1000), //turns to face hole
                        new AutonIndexer(indexer, 1000), //lets preloads into flywheel to shoot them
                        new TurnSpeedPos(driveTrain, sensors, 120, 125, 2000), //turns left facing rendevous point
                        new DriveForwardSpeedPos(driveTrain, 40 * Constants.IN_SEC_TO_TICS_TENTH, 135 * Constants.ENCODER_TICS_PER_INCH, 5000),
                        new TurnSpeedPos(driveTrain, sensors, 120, 105, 2000),
                        new ParallelCommandGroup( //at edge of rendevous point, turn to face hole and make flywheel spin faster
                            new DriveForwardSpeedPos(driveTrain, 40 * Constants.IN_SEC_TO_TICS_TENTH, 32 * Constants.ENCODER_TICS_PER_INCH, 1000), 
                            new AutonIntake(intakeSystem, 1000)
                        ),
                        new DriveForwardSpeedPos(driveTrain, 40 * Constants.IN_SEC_TO_TICS_TENTH, -18 * Constants.ENCODER_TICS_PER_INCH, 1000),
                        new TurnSpeedPos(driveTrain, sensors, 120, 115, 2000),
                        new LimeLightTurn(driveTrain, limeLight, 1000), //turns to face hole
                        new AutonIndexer(indexer, 1000)
            // new AutonIndexer(indexer, 1000) //shoot three balls from near rendevous point
        );
    }

    

}