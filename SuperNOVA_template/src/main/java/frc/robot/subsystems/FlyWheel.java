package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class FlyWheel extends SubsystemBase {

    public final WPI_TalonSRX flyWheelMotor;

    public FlyWheel(WPI_TalonSRX flyWheel) {
        this.flyWheelMotor = flyWheel;
    }

}