package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class FlywheelHood extends SubsystemBase {

    public final WPI_TalonSRX flywheelHoodMotor;

    public FlywheelHood(WPI_TalonSRX flywheelHood) {
        this.flywheelHoodMotor = flywheelHood;
    }

}