package frc.robot.subsystems;

import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class Lift extends SubsystemBase
{
    public final SpeedController liftMotor;
    public final SpeedController roller;

    public Lift(SpeedController lift, SpeedController roller)
    {
        this.liftMotor = lift;
        this.roller = roller;
    }
}