package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class PneumaticHood extends SubsystemBase {

    public final DoubleSolenoid hoodPiston;

    public PneumaticHood(DoubleSolenoid ds) {
        this.hoodPiston = ds;
    }
}